export interface ProductVariation {
    id?: number;
    price: number;
    name: string;
    product: number;
}

export class DefaultProductValiation implements ProductVariation {
    id?: number;
    price: 0;
    name: '';
    product: 0;
    constructor(variations?: ProductVariation) {
        Object.assign(this, variations);
    }
}
