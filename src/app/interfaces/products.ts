export interface Product {
    id?: number;
    name: string;
    image: string;
    type: number;
    readonly?: boolean;
    active: boolean;
    description?: string;
}

export class DefaultProduct implements Product {
    name = '';
    image = '';
    type = 1;
    active = false;
    description = '';
    constructor(productType?: Product) {
        Object.assign(this, productType);
    }
}
