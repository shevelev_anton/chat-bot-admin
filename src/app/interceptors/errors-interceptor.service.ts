import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
} from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '@modules/auth/services/auth.service';
import { MessageDialogAction } from '@reducers/message-dialog-action';
import { MESSAGE_ERROR } from '@const/message-dialog-types';

@Injectable({
    providedIn: 'root',
})
export class ErrorsInterceptor implements HttpInterceptor {
    constructor(public store: Store<any>, private authService: AuthService) {}

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(req)
        .pipe(
            catchError((error) => {
                this.handleError(error.status);
                return EMPTY;
            })
        );
    }

    handleError(statusCode): void {
        const machError = {
            400: () =>
                this.showMessage(
                    'Bad request. Check request params or connect your administrator'
                ),
            401: () => this.userUnauthorized(),
            404: () => this.showMessage('Requested data not found'),
            default: () => this.showMessage('Oops! Unexpected error.'),
        };
        return (machError[statusCode] || machError.default)();
    }

    userUnauthorized(): void {
        let message = 'Login or password is incorrect';
        if (this.authService.getCredentials()) {
            this.authService.logout();
            message = 'Your credentials is expired. Please login';
        }
        this.showMessage(message);
    }

    showMessage(message: string): void {
        this.store.dispatch(
            MessageDialogAction({
                visible: true,
                dialogType: MESSAGE_ERROR,
                message,
            })
        );
    }
}
