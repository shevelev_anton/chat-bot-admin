import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
} from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Spinner } from '@reducers/spinner-action';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root',
})
export class LoaderInterceptor implements HttpInterceptor {
    constructor(public store: Store<any>) {}
    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        setTimeout(() => {
            this.store.dispatch(Spinner({ visible: true }));
        }, 0);
        return next.handle(req).pipe(
            finalize(() => {
                setTimeout(() => {
                    this.store.dispatch(Spinner({ visible: false }));
                }, 0);
            })
        );
    }
}
