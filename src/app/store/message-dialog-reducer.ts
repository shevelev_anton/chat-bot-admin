import { createReducer, on, Action } from '@ngrx/store';
import {
    defaultMessageDialog,
    MessageDialogAction,
} from './message-dialog-action';
import { MessageDialog } from '@actions';

const messageDialogReducer = createReducer(
    defaultMessageDialog,
    on(MessageDialogAction, (state, props) => {
        return props;
    })
);

export function message(state: MessageDialog, action: Action) {
    return messageDialogReducer(state, action);
}
