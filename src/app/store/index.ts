import { spinner } from './spinner-reducer';
import { message } from './message-dialog-reducer';

export const store = {
    spinner,
    message
};
