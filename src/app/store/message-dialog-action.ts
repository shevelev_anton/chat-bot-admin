import { createAction, props } from '@ngrx/store';
import { MessageDialogType, MessageDialog } from '@actions';
import { MESSAGE_WARNING } from '@const/message-dialog-types';

export const MessageDialogAction = createAction(
    MessageDialogType,
    props<MessageDialog>()
);

export class MessageDialogState implements MessageDialog {
    visible = false;
    dialogType = '';
    message = '';
    constructor(params?: MessageDialog) {
        this.visible = params ? params.visible : false;
        this.dialogType = params ? params.dialogType : MESSAGE_WARNING;
        this.message = params ? params.message : '';
    }
}

export const defaultMessageDialog = new MessageDialogState();
