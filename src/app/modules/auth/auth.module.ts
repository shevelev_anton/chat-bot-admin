import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthRouterComponent } from './auth-router/auth-router.component';
import { AuthRoutingModule } from './auth-routing.module';
import { SignUpCompleteComponent } from './sign-up-complete/sign-up-complete.component';
import { AppCommonModule } from '@modules/app-common/app-common.module';
import { AuthRoutes } from './auth-routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { Urls } from '@const/urls';
import { AuthGuard } from './guards/auth.guard';

const EXPORTS = [AuthRouterComponent];
@NgModule({
    declarations: [
        EXPORTS,
        SignInComponent,
        SignUpComponent,
        SignUpCompleteComponent,
    ],
    imports: [
        CommonModule,
        AuthRoutingModule,
        AppCommonModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [EXPORTS],
    providers: [AuthRoutes, AuthService, AuthGuard, Urls],
})
export class AuthModule {}
