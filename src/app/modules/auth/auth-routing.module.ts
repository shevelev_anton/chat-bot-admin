import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SIGN_IN, SIGN_UP, SIGN_UP_COMPLETE } from './auth-routes';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignUpCompleteComponent } from './sign-up-complete/sign-up-complete.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: SIGN_IN,
        pathMatch: 'full',
    },
    {
        path: SIGN_IN,
        component: SignInComponent,
    },
    {
        path: SIGN_UP,
        component: SignUpComponent,
    },    {
        path: SIGN_UP_COMPLETE,
        component: SignUpCompleteComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AuthRoutingModule {}
