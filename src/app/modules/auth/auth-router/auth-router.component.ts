import { Component, OnInit } from '@angular/core';
import { AuthRoutes } from '../auth-routes';

@Component({
    selector: 'app-auth-router',
    templateUrl: './auth-router.component.html',
    styleUrls: ['./auth-router.component.scss'],
})
export class AuthRouterComponent implements OnInit {
    constructor(private authRoutes: AuthRoutes) {}

    ngOnInit(): void {}

    navirateToStartPage(): void {
        this.authRoutes.closeAuthPage();
    }
}
