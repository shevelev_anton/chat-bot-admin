import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthRoutes } from '../auth-routes';
import { AuthService } from '../services/auth.service';
import { AppRoutes } from '@modules/app-core/app-routes';

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.scss', '../common-styles.scss'],
})
export class SignInComponent implements OnInit, OnDestroy {
    public isLoginFocused = false;
    public isPasswordFocused = false;
    public signInForm: FormGroup;
    public submited = false;

    constructor(
        public authRoutes: AuthRoutes,
        public appRoutes: AppRoutes,
        private fb: FormBuilder,
        private auth: AuthService
    ) {}

    ngOnInit(): void {
        this.signInForm = this.fb.group({
            username: [null, [Validators.required]],
            password: [null, [Validators.required]],
        });
    }

    onSubmit(f: FormGroup): void {
        this.submited = true;
        if (f.invalid) {
            return;
        }
        this.auth.login(f.value, () => {
            this.submited = false;
            f.get('password').reset();
        });
    }

    close() {
        this.authRoutes.closeAuthPage();
    }

    ngOnDestroy(): void {
        this.signInForm.reset();
    }
}
