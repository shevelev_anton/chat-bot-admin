import { Component, OnInit } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';

@Component({
    template: '',
})
export class NgModelComponent implements ControlValueAccessor {
    private $model: any = null;
    constructor() {}

    onChange: any = () => {};
    onTouch: any = () => {};
    set model(model) {
        // this value is updated by programmatic changes if( val !== undefined && this.val !== val){
        this.$model = model;
        this.onChange(model);
        this.onTouch(model);
    }
    get model() {
        // this value is updated by programmatic changes if( val !== undefined && this.val !== val){
        return this.$model;
    }

    // this method sets the value programmatically
    writeValue(value: any) {
        this.model = value;
    }
    // upon UI element value changes, this method gets triggered
    registerOnChange(fn: any) {
        this.onChange = fn;
    }
    // upon touching the element, this method gets triggered
    registerOnTouched(fn: any) {
        this.onTouch = fn;
    }
}
