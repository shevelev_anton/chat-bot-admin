import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartPageComponent } from './start-page/start-page.component';
import { SignInSignUpSwitcher } from './sign-in-sign-up-switcher/sign-in-sign-up-switcher.component';
import { RouterModule } from '@angular/router';
import { UserBoxComponent } from './user-box/user-box.component';
import { InputComponent } from './input/input.component';
import { NgModelComponent } from './ng-model/ng-model.component';
import { FormsModule } from '@angular/forms';
import { SpinnerComponent } from './spinner/spinner.component';
import { MessageDialodComponent } from './message-dialod/message-dialod.component';
import { FileUploaderComponent } from './file-uploader/file-uploader.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const EXPORTS = [
    StartPageComponent,
    UserBoxComponent,
    InputComponent,
    SpinnerComponent,
    MessageDialodComponent,
    FileUploaderComponent,
    CheckboxComponent,
];

@NgModule({
    declarations: [EXPORTS, SignInSignUpSwitcher, NgModelComponent],
    imports: [CommonModule, RouterModule, FormsModule],
    exports: [EXPORTS],
})
export class AppCommonModule {}
