import {
    Component,
    OnInit,
    ViewChild,
    ElementRef,
    Input,
    Output,
    EventEmitter,
} from '@angular/core';

@Component({
    selector: 'app-file-uploader',
    templateUrl: './file-uploader.component.html',
    styleUrls: ['./file-uploader.component.scss'],
})
export class FileUploaderComponent implements OnInit {
    @ViewChild('file', { static: false }) file: ElementRef;
    @Input() image: string | ArrayBuffer = '';
    @Output() updateImage: EventEmitter<File> = new EventEmitter();
    constructor() {}

    ngOnInit(): void {}

    onFileChanged(event): void {

        if (!event.target.files.length) {
            return;
        }
        const reader = new FileReader();

        reader.onload = e => {
            this.image = e.target.result;
        };
        const file: File = event.target.files[0];
        // this.image = file;
        this.updateImage.emit(file);
        reader.readAsDataURL(file);
        this.file.nativeElement.value = '';
    }
}
