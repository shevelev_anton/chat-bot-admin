import { Component, OnInit, HostBinding } from '@angular/core';
import { NgModelComponent } from '../ng-model/ng-model.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-checkbox',
    templateUrl: './checkbox.component.html',
    styleUrls: ['./checkbox.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: CheckboxComponent,
            multi: true,
        },
    ],
})
export class CheckboxComponent extends NgModelComponent implements OnInit {
    @HostBinding('class.active') model;
    constructor() {
        super();
    }

    ngOnInit(): void {}
}
