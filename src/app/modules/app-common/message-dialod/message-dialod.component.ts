import {
    Component,
    OnInit,
    Input,
    OnDestroy,
    HostBinding,
} from '@angular/core';
import {
    MessageDialogAction,
    defaultMessageDialog,
} from '@reducers/message-dialog-action';
import { Store } from '@ngrx/store';

@Component({
    selector: 'app-message-dialod',
    templateUrl: './message-dialod.component.html',
    styleUrls: ['./message-dialod.component.scss'],
})
export class MessageDialodComponent implements OnInit, OnDestroy {
    @Input() message = '';
    @HostBinding('class') @Input() type;
    @HostBinding('class.show') isShown = false;
    private timeout;
    constructor(private store: Store<any>) {}

    ngOnInit(): void {
        this.timeout = setTimeout(() => {
            this.isShown = true;
        }, 0);
        this.timeout = setTimeout(() => {
            this.hideMessage();
        }, 10000);
    }
    hideMessage(): void {
        this.isShown = false;
        this.timeout = setTimeout(() => {
            this.store.dispatch(MessageDialogAction(defaultMessageDialog));
        }, 250);
    }
    ngOnDestroy(): void {
        clearTimeout(this.timeout);
    }
}
