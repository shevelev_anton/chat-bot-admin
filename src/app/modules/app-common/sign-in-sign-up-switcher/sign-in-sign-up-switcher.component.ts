import { Component, OnInit } from '@angular/core';
import { SIGN_IN, SIGN_UP, AuthRoutes } from '@modules/auth/auth-routes';

@Component({
    // tslint:disable-next-line: component-selector
    selector: 'sign-in-sign-up-switcher',
    templateUrl: './sign-in-sign-up-switcher.component.html',
    styleUrls: ['./sign-in-sign-up-switcher.component.scss'],
})
// tslint:disable-next-line: component-class-suffix
export class SignInSignUpSwitcher implements OnInit {
    public routes = [
        {
            name: 'Sign In',
            route: this.authRoutes.SIGN_IN,
        },
        {
            name: 'Sign Up',
            route: this.authRoutes.SIGN_UP,
        },
    ];
    constructor(private authRoutes: AuthRoutes) {}

    ngOnInit(): void {}
}
