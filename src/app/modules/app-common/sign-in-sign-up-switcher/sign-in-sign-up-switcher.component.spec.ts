import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInSignUpSwitcherComponent } from './sign-in-sign-up-switcher.component';

describe('SignInSignUpSwitcherComponent', () => {
  let component: SignInSignUpSwitcherComponent;
  let fixture: ComponentFixture<SignInSignUpSwitcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignInSignUpSwitcherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInSignUpSwitcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
