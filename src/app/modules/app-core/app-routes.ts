export const START = 'start';
export const PAGE_NOT_FOUND = 'page-not-found';

export class AppRoutes {
    public START = `/${START}`;
    public PAGE_NOT_FOUND = `/${PAGE_NOT_FOUND}`;
}
