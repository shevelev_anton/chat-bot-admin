import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app-core-router/app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppCommonModule } from '@modules/app-common/app-common.module';
import { AuthModule } from '@modules/auth/auth.module';
import { AdminModule } from '@modules/admin/admin.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpService } from '@services/http.service';
import { StoreModule } from '@ngrx/store';
import { store } from '@store';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutes } from './app-routes';
import { LoaderInterceptor } from '@interceptors/loader-interceptor.service';
import { ErrorsInterceptor } from '@interceptors/errors-interceptor.service';
import { HttpInterceptor } from '@interceptors/http-interceptor.service';

@NgModule({
    declarations: [AppComponent, PageNotFoundComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        AppCommonModule,
        AuthModule,
        AdminModule,
        HttpClientModule,
        StoreModule.forRoot(store),
    ],
    providers: [
        HttpService,
        AppRoutes,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: LoaderInterceptor,
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorsInterceptor,
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpInterceptor,
            multi: true,
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
