import { Injectable } from '@angular/core';
import { ProductStore } from './product-store.service';
import { HttpService } from '@services/http.service';
import { Urls } from '@const/urls';
import { ProductType, DefaultProductType } from '@interface/product-types';
import { Product, DefaultProduct } from '@interface/products';
import { Observable } from 'rxjs';
import {
    ProductVariation,
    DefaultProductValiation,
} from '@interface/product-variations';

@Injectable({
    providedIn: 'root',
})
export class ProductService {
    constructor(
        private productStore: ProductStore,
        private http: HttpService,
        private urls: Urls
    ) {}

    /** PRODUCT */
    retrieveProducts(): void {
        this.http.get(this.urls.PRODUCT).subscribe((products: Product[]) => {
            this.productStore.products.value = products.map((product) => {
                return new DefaultProduct(product);
            });
        });
    }
    retrieveProductsById(id: number): Observable<Product> {
        return this.http.get(`${this.urls.PRODUCT}${id}`);
    }
    addProduct(product: Product): Observable<Product> {
        const data = new FormData();
        data.append('image', product.image);
        data.append('name', product.name);
        data.append('type', product.type.toString());
        data.append('description', product.description);
        return this.http.post(this.urls.PRODUCT, data);
    }
    deleteProduct(product: Product): void {
        this.http
            .delete(`${this.urls.PRODUCT}${product.id}/`)
            .subscribe((deleteProduct) => {
                this.retrieveProducts();
            });
    }
    updateProduct(product: Product): void {
        const data = new FormData();
        data.append('image', product.image);
        data.append('name', product.name);
        data.append('type', product.type.toString());
        data.append('description', product.description);
        this.http
            .put(`${this.urls.PRODUCT}${product.id}`, data)
            .subscribe((updatedProduct: Product) => {
                console.log(updatedProduct);
            });
    }
    /** PRODUCT */

    /** PRODUCT VARIATIONS */
    retrieveProductVariations(productId: number): void {
        this.http
            .get(`${this.urls.PRODUCT_VARIATION}?product=${productId}`)
            .subscribe((productVariations: ProductVariation[]) => {
                this.productStore.productVariations.value = productVariations.map(
                    (productVariation) => {
                        return new DefaultProductValiation(productVariation);
                    }
                );
            });
    }
    addProductVariation(productVariation: ProductVariation): void {
        this.http
            .post(this.urls.PRODUCT_VARIATION, productVariation)
            .subscribe((addProductVariation) => {
                console.log(addProductVariation);
                this.retrieveProductVariations(productVariation.product);
            });
    }
    updateProductVariation(productVariation: ProductVariation): void {
        this.http
            .put(
                `${this.urls.PRODUCT_VARIATION}${productVariation.id}/`,
                productVariation
            )
            .subscribe((addProductVariation: ProductVariation) => {
                this.retrieveProductVariations(productVariation.product);
            });
    }
    deleteProductVariation(productVariation: ProductVariation): void {
        this.http
            .delete(`${this.urls.PRODUCT_VARIATION}${productVariation.id}`)
            .subscribe((addProductVariation) => {
                this.retrieveProductVariations(productVariation.product);
            });
    }
    /** PRODUCT VARIATIONS */

    /** PRODUCT TUPES */
    retrieveProductTypes(): void {
        this.http
            .get(this.urls.PRODUCT_TYPE)
            .subscribe((productTypes: ProductType[]) => {
                this.productStore.productTypes.value = productTypes.map(
                    (productType) => {
                        return new DefaultProductType(productType);
                    }
                );
            });
    }

    addProductType(productType: ProductType): void {
        this.http
            .post(this.urls.PRODUCT_TYPE, productType)
            .subscribe((addProductType) => {
                this.retrieveProductTypes();
            });
    }
    deleteProductType(productType: ProductType): void {
        this.http
            .delete(`${this.urls.PRODUCT_TYPE}${productType.id}`)
            .subscribe((deleteProductType) => {
                this.retrieveProductTypes();
            });
    }
    updateProductType(productType: ProductType): void {
        const { name } = productType;
        this.http
            .put(`${this.urls.PRODUCT_TYPE}${productType.id}/`, { name })
            .subscribe((updateProductType) => {
                this.retrieveProductTypes();
            });
    }
    /** PRODUCT TUPES */
}
