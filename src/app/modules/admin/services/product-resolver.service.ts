import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Resolve } from '@angular/router';
import { HttpService } from '@services/http.service';
import { Urls } from '@const/urls';
import { map } from 'rxjs/operators';
import { DefaultProduct } from '@interface/products';

@Injectable({
  providedIn: 'root'
})
export class ProductResolverService implements Resolve<Observable<any>> {

  constructor(
    private http: HttpService,
    private urls: Urls) { }
  resolve(): Observable<any> {
    return this.http.get(this.urls.PRODUCT).pipe(map((product) => {
      return new DefaultProduct(product);
    }));
  }
}
