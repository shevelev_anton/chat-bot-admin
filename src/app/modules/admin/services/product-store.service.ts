import { Injectable } from '@angular/core';
import { ProductType } from '@interface/product-types';
import { CustomSubject } from '@models/_subject';

@Injectable({
    providedIn: 'root',
})
export class ProductStore {
    public productTypes: CustomSubject<ProductType[]> = new CustomSubject([]);
    public products: CustomSubject<ProductType[]> = new CustomSubject([]);
    public productVariations: CustomSubject<ProductType[]> = new CustomSubject([]);

    constructor() {}

    getProductTypeById(id: number): ProductType | null {
        const typeById = this.productTypes.value.filter((type: any) => {
            return type.id === id;
        });
        if (typeById.length) {
            return typeById[0];
        }
        return null;
    }
}
