import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { ProductStore } from '../../services/product-store.service';
import { ProductType, DefaultProductType } from '@interface/product-types';
import { cloneDeep } from 'lodash';
@Component({
    selector: 'app-product-types',
    templateUrl: './product-types.component.html',
    styleUrls: ['./product-types.component.scss'],
})
export class ProductTypesComponent implements OnInit {
    public productTypes: ProductType[] = [];
    private productTypeCopy: ProductType;
    public newProductType: ProductType = new DefaultProductType();
    constructor(
        private productService: ProductService,
        public productStore: ProductStore
    ) {}

    ngOnInit(): void {
        this.productStore.productTypes.getAsync().subscribe(productTypes => {
            this.productTypes = productTypes;
        });
    }
    editProductType(productType: ProductType): void {
        this.productTypeCopy = cloneDeep(productType);
        productType.readonly = false;
    }
    deleteProductType(productType: ProductType): void {
        this.productService.deleteProductType(productType);
    }
    updateProductType(productType: ProductType): void {
        console.log(productType);
        this.productService.updateProductType(productType);
    }

    cancelEditingProductType(productType: ProductType, index: number): void {
        console.log(this.productTypeCopy);
        this.productTypes[index] = cloneDeep(this.productTypeCopy);
    }
    addNewType(): void {
        this.newProductType.readonly = true;
        console.log(this.newProductType);
        this.productService.addProductType(this.newProductType);
    }
    cancelAddNewType(): void {
        this.newProductType = new DefaultProductType();
    }

    showAddingBox(): void {
        this.newProductType = new DefaultProductType();
        this.newProductType.readonly = false;
    }

    trackProductTypeById(index: number, el: ProductType): number {
        return el.id;
    }
}
