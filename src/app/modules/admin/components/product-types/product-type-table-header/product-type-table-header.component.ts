import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-type-table-header',
  templateUrl: './product-type-table-header.component.html',
  styleUrls: ['./product-type-table-header.component.scss', '../../../common-styles.scss']
})
export class ProductTypeTableHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
