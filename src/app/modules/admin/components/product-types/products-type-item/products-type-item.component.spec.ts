import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsTypeItemComponent } from './products-type-item.component';

describe('ProductsTypeItemComponent', () => {
  let component: ProductsTypeItemComponent;
  let fixture: ComponentFixture<ProductsTypeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsTypeItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsTypeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
