import { Component, OnInit, Input } from '@angular/core';
import { NgModelComponent } from '@modules/app-common/ng-model/ng-model.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-products-type-item',
    templateUrl: './products-type-item.component.html',
    styleUrls: ['./products-type-item.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: ProductsTypeItemComponent,
            multi: true,
        },
    ],
})
export class ProductsTypeItemComponent extends NgModelComponent
    implements OnInit {
    @Input() readonly = true;

    constructor() {
        super();
    }
    ngOnInit(): void {
    }
}
