import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
    selector: 'app-btn-group',
    templateUrl: './btn-group.component.html',
    styleUrls: ['./btn-group.component.scss'],
})
export class BtnGroupComponent implements OnInit {
    @Input() editable = false;
    @Output() edit = new EventEmitter();
    @Output() delete = new EventEmitter();
    @Output() save = new EventEmitter();
    @Output() cancel = new EventEmitter();
    constructor() {}

    ngOnInit(): void {}
}
