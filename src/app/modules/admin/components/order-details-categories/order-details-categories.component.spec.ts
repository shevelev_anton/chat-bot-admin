import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDetailsCategoriesComponent } from './order-details-categories.component';

describe('OrderDetailsCategoriesComponent', () => {
  let component: OrderDetailsCategoriesComponent;
  let fixture: ComponentFixture<OrderDetailsCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDetailsCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDetailsCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
