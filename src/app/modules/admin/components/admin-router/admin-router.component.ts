import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';

@Component({
    selector: 'app-admin-router',
    templateUrl: './admin-router.component.html',
    styleUrls: ['./admin-router.component.scss'],
})
export class AdminRouterComponent implements OnInit {
    constructor(private productService: ProductService) {}

    ngOnInit(): void {
        this.productService.retrieveProductTypes();
        this.productService.retrieveProducts();
    }
}
