import { Component, OnInit, Input } from '@angular/core';
import { NgModelComponent } from '@modules/app-common/ng-model/ng-model.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ProductStore } from '@modules/admin/services/product-store.service';

@Component({
    selector: 'app-products-item',
    templateUrl: './products-item.component.html',
    styleUrls: [
        './products-item.component.scss',
        '../../../common-styles.scss',
    ],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: ProductsItemComponent,
            multi: true,
        },
    ],
})
export class ProductsItemComponent extends NgModelComponent implements OnInit {
    @Input() readonly = true;

    constructor(private productStore: ProductStore) {
        super();
    }

    ngOnInit(): void {}

    getProductTypeName(type: number): string {
        if (this.productStore.productTypes.value && type) {
            const typeName = this.productStore.productTypes.value.filter(
                productType => {
                    return productType.id === type;
                }
            );
            if (typeName.length) {
                return typeName[0].name;
            }
        }
        return '';
    }
    formatString(notFormatedString: string): string {
        return notFormatedString
            ? notFormatedString.replace('<br />', '').replace('<br/>', '')
            : notFormatedString;
    }
}
