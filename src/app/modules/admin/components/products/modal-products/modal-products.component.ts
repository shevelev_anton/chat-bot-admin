import {
    Component,
    OnInit,
    EventEmitter,
    Output,
    Input,
    OnDestroy,
    ViewChild,
    ElementRef,
} from '@angular/core';
import { ProductStore } from '@modules/admin/services/product-store.service';
import { Product, DefaultProduct } from '@interface/products';
import { ProductService } from '@modules/admin/services/product.service';
import {
    ProductVariation,
    DefaultProductValiation,
} from '@interface/product-variations';
import { Subject, combineLatest } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '@services/http.service';
import { Store } from '@ngrx/store';
import { MessageDialogAction } from '@reducers/message-dialog-action';
import { MESSAGE_WARNING } from '@const/message-dialog-types';

@Component({
    selector: 'app-modal-products',
    templateUrl: './modal-products.component.html',
    styleUrls: ['./modal-products.component.scss', '../../../common-styles.scss'],
})
export class ModalProductsComponent implements OnInit, OnDestroy {
    @Output() closeModal: EventEmitter<any> = new EventEmitter();
    @Input() product: Product = new DefaultProduct();
    public editProductForm: FormGroup;
    private unsibscribe: Subject<any> = new Subject();

    public newProductVariation: ProductVariation = new DefaultProductValiation();
    public isNewProductVariation = false;

    constructor(
        public productStore: ProductStore,
        private productService: ProductService,
        private fb: FormBuilder,
        private store: Store<any>
    ) {}

    ngOnInit(): void {
        if (this.product.id) {
            this.productService.retrieveProductVariations(this.product.id);
        }
        this.productStore.productVariations.getAsync().subscribe(() => {
            this.isNewProductVariation = false;
        });
        this.editProductForm = this.fb.group({
            name: [this.product.name, [Validators.required]],
            image: [this.product.image, [Validators.required]],
            type: [this.product.type, [Validators.required]],
            active: [this.product.active],
            description: [this.product.description, [Validators.required]],
        });
    }

    save(): void {
        if (this.editProductForm.invalid) {
            console.log(this.editProductForm, 'invalid');
            this.store.dispatch(
                MessageDialogAction({
                    visible: true,
                    dialogType: MESSAGE_WARNING,
                    message: 'Please fill all required fields',
                })
            );
            return;
        }
        if (this.product.id) {
            this.productService.updateProduct(this.product);
        } else {
            this.productService
                .addProduct(this.editProductForm.value)
                .subscribe((product: Product) => {
                    this.productService.retrieveProducts();
                    this.productService
                        .retrieveProductsById(product.id)
                        .subscribe((productById: Product) => {
                            this.product = productById;
                        });
                });
        }
    }
    cancel(): void {
        this.product = new DefaultProduct();
        this.closeModal.emit(null);
    }

    addNewProductVariation(): void {
        this.isNewProductVariation = true;
    }
    updateImage(event): void {
        this.product.image = event;
        this.editProductForm.get('image').setValue(event);
    }
    ngOnDestroy(): void {
        this.product = new DefaultProduct();
        this.unsibscribe.next();
        this.unsibscribe.complete();
    }
}
