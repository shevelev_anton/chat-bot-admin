import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { ProductType } from '@interface/product-types';
import { ProductStore } from '../../services/product-store.service';
import { Product, DefaultProduct } from '@interface/products';
import { cloneDeep } from 'lodash';
import { ActivatedRoute } from '@angular/router';
@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
    public products: Product[] = [];
    public selectedProduct: Product = new DefaultProduct();
    public isAppProductModal = false;
    constructor(
        private productService: ProductService,
        private productStore: ProductStore,
        private route: ActivatedRoute
    ) { }

    ngOnInit(): void {
        this.productStore.products
            .getAsync()
            .subscribe((products: Product[]) => {
                this.products = products.map(product => {
                    return new DefaultProduct(product);
                });
            });
        console.log(this.route.snapshot.data);

    }

    openAddProductModal(): void {
        this.selectedProduct = new DefaultProduct();
        this.isAppProductModal = true;
    }
    closeModal(): void {
        this.isAppProductModal = false;
    }

    trackByProductId(index: number, el: ProductType): number {
        return el.id;
    }
    editProduct(product: Product): void {
        this.selectedProduct = cloneDeep(product);
        this.isAppProductModal = true;
    }
    deleteProduct(product: Product): void {
        this.productService.deleteProduct(product);
    }
}
