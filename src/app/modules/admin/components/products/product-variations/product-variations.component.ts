import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
    ProductVariation,
    DefaultProductValiation,
} from '@interface/product-variations';
import { cloneDeep } from 'lodash';
import { ProductService } from '@modules/admin/services/product.service';
import { Product } from '@interface/products';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MessageDialogAction } from '@reducers/message-dialog-action';
import { MESSAGE_WARNING } from '@const/message-dialog-types';
@Component({
    selector: 'app-product-variations',
    templateUrl: './product-variations.component.html',
    styleUrls: [
        './product-variations.component.scss',
        '../../../common-styles.scss',
    ],
})
export class ProductVariationsComponent implements OnInit {
    @Input()
    productVariation: ProductVariation = new DefaultProductValiation();
    @Input() product: Product;
    @Output() cancelEditing: EventEmitter<null> = new EventEmitter();
    public productVariationCopy: ProductVariation = new DefaultProductValiation();
    public readonly = true;
    public productVariationForm: FormGroup;
    constructor(
        private productService: ProductService,
        private fb: FormBuilder,
        private store: Store<any>
    ) {}

    ngOnInit(): void {
        this.productVariationForm = this.fb.group({
            name: [this.productVariation.name, [Validators.required]],
            price: [this.productVariation.price, [Validators.required]],
        });
        if (!this.productVariation.id) {
            this.readonly = false;
        }
    }
    editProductVariation(): void {
        this.productVariationCopy = cloneDeep(this.productVariation);
        this.readonly = false;
    }
    deleteProductVariation(): void {
        this.productService.deleteProductVariation(this.productVariation);
    }
    saveProductVariation(): void {
        console.log(this.productVariation);
        if (this.productVariationForm.invalid) {
            this.store.dispatch(
                MessageDialogAction({
                    visible: true,
                    dialogType: MESSAGE_WARNING,
                    message: 'Please fill all required fields',
                })
            );
            return;
        }
        if (this.productVariation.product) {
            console.log(1);
            this.productService.updateProductVariation(this.productVariation);
        } else {
            console.log(2);
            this.productVariation.product = this.product.id;
            this.productService.addProductVariation(this.productVariation);
        }
    }
    cancelEditProductVariation(): void {
        this.readonly = true;
        this.productVariation = cloneDeep(this.productVariationCopy);
        this.cancelEditing.emit();
    }
}
