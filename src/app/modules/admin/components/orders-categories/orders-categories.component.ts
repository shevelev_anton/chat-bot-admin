import { Component, OnInit, Input } from '@angular/core';

@Component({
    // tslint:disable-next-line: component-selector
    selector: '[app-orders-categories]',
    templateUrl: './orders-categories.component.html',
    styleUrls: ['./orders-categories.component.scss'],
})
export class OrdersCategoriesComponent implements OnInit {
    public selectedValue: string;
    @Input() order: any = null;

    constructor() {}

    ngOnInit(): void {}
}
