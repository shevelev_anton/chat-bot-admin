import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import {
    MessageDialogAction,
    defaultMessageDialog,
} from '@reducers/message-dialog-action';
import {
    MESSAGE_WARNING,
    MESSAGE_ERROR,
    MESSAGE_SUCCESS,
} from '@const/message-dialog-types';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
    constructor(private store: Store<any>) {}

    ngOnInit(): void {}

    showWarning(): void {
        this.show(MESSAGE_WARNING, 'WARNING');
    }
    showError(): void {
        this.show(MESSAGE_ERROR, 'ERROR');
    }
    showSuccess(): void {
        this.show(MESSAGE_SUCCESS, 'SUCCESSSUCCESSSUCCESSSUCCESS SUCCESSSUCCESSSUCCESSSUCCESS SUCCESSSUCCESSSUCCESSSUCCESS');
    }
    show(type: string, message: string): void {
        this.store.dispatch(
            MessageDialogAction({
                visible: true,
                dialogType: type,
                message,
            })
        );
    }
    hide(): void {
        this.store.dispatch(MessageDialogAction(defaultMessageDialog));
    }
}
