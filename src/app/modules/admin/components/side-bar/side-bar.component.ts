import { Component, OnInit } from '@angular/core';
import { AdminRoutes } from '../../admin-routes';
import { Router } from '@angular/router';

@Component({
    selector: 'app-side-bar',
    templateUrl: './side-bar.component.html',
    styleUrls: ['./side-bar.component.scss'],
})
export class SideBarComponent implements OnInit {
    public folders = [
        // {
        //     name: 'Users',
        //     subRoutes: [
        //         {
        //             route: this.adminRoutes.USERS,
        //             name: 'Users list',
        //         }
        //     ],
        // },
        {
            name: 'Product',
            subRoutes: [
                {
                    route: this.adminRoutes.PRODUCT_TYPES,
                    name: 'Product Types',
                },
                {
                    route: this.adminRoutes.PRODUCTS,
                    name: 'Products',
                },
            ],
        },
        // {
        //     name: 'Orders',
        //     subRoutes: [
        //         {
        //             route: this.adminRoutes.ORDERS,
        //             name: 'Orders',
        //         },
        //     ],
        // },
    ];
    constructor(private adminRoutes: AdminRoutes, private router: Router) {}

    ngOnInit(): void {}
    navigate(event, route): void {
        // event.stopPropagation();
        event.stopImmediatePropagation();
        this.router.navigate([route]);
    }
}
