import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRouterComponent } from './components/admin-router/admin-router.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { ProductsComponent } from './components/products/products.component';
import { OrdersComponent } from './components/orders/orders.component';
import { UsersComponent } from './components/users/users.component';
import { AdminRoutingModule } from './admin-routing.module';
import { AppCommonModule } from '@modules/app-common/app-common.module';
import { AdminRoutes } from './admin-routes';
import { OrderDetailsComponent } from './components/order-details/order-details.component';
import { ProductTypesComponent } from './components/product-types/product-types.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BtnGroupComponent } from './components/btn-group/btn-group.component';
import { ProductsTypeItemComponent } from './components/product-types/products-type-item/products-type-item.component';
import { BtnAddComponent } from './components/btn-add/btn-add.component';
import { OrdersCategoriesComponent } from './components/orders-categories/orders-categories.component';
import { OrderDetailsCategoriesComponent } from './components/order-details-categories/order-details-categories.component';
import { ModalProductsComponent } from './components/products/modal-products/modal-products.component';
import { ProductTableHeaderComponent } from './components/products/product-table-header/product-table-header.component';
import { ProductsItemComponent } from './components/products/products-item/products-item.component';
import { ProductVariationsComponent } from './components/products/product-variations/product-variations.component';
import { ProductTypeTableHeaderComponent } from './components/product-types/product-type-table-header/product-type-table-header.component';

const EXPORTS = [AdminRouterComponent];

@NgModule({
    declarations: [
        EXPORTS,
        SideBarComponent,
        ProductsComponent,
        OrdersComponent,
        UsersComponent,
        OrderDetailsComponent,
        ProductTypesComponent,
        BtnGroupComponent,
        ProductsItemComponent,
        ProductsTypeItemComponent,
        BtnAddComponent,
        OrdersCategoriesComponent,
        OrderDetailsCategoriesComponent,
        ModalProductsComponent,
        ProductTableHeaderComponent,
        ProductVariationsComponent,
        ProductTypeTableHeaderComponent,
    ],
    imports: [
        CommonModule,
        AdminRoutingModule,
        AppCommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [EXPORTS],
    providers: [AdminRoutes],
})
export class AdminModule {}
