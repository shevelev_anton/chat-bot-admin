import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
    USERS,
    PRODUCTS,
    ORDERS,
    PRODUCT_TYPES,
    ORDER_DETAILS,
} from './admin-routes';
import { UsersComponent } from './components/users/users.component';
import { ProductsComponent } from './components/products/products.component';
import { OrdersComponent } from './components/orders/orders.component';
import { ProductTypesComponent } from './components/product-types/product-types.component';
import { OrderDetailsComponent } from './components/order-details/order-details.component';
import { ProductResolverService } from './services/product-resolver.service';

const routes: Routes = [
    {
        path: '',
        // redirectTo: USERS,
        redirectTo: PRODUCTS,
        pathMatch: 'full',
    },
    {
        path: USERS,
        component: UsersComponent,
    },
    {
        path: PRODUCTS,
        component: ProductsComponent,
        resolve: {
            products: ProductResolverService
        }
    },
    {
        path: PRODUCT_TYPES,
        component: ProductTypesComponent,
    },
    {
        path: ORDERS,
        component: OrdersComponent,
    },
    {
        path: ORDER_DETAILS,
        component: OrderDetailsComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AdminRoutingModule { }
