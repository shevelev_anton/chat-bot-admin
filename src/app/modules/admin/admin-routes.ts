export const ADMIN = 'admin';
export const USERS = 'users';
export const PRODUCTS = 'products';
export const PRODUCT_TYPES = 'product-types';
export const ORDERS = 'orders';
export const ORDER_DETAILS = 'order-details';


export class AdminRoutes {
    public USERS = `/${ADMIN}/${USERS}`;
    public PRODUCTS = `/${ADMIN}/${PRODUCTS}`;
    public PRODUCT_TYPES = `/${ADMIN}/${PRODUCT_TYPES}`;
    public ORDERS = `/${ADMIN}/${ORDERS}`;
    public ORDER_DETAILS = `/${ADMIN}/${ORDER_DETAILS}`;

}
