import { environment } from '@environments/environment';

export class Urls {
    private SERVER_ROOT = environment.serverUrl;
    public LOGIN = `${this.SERVER_ROOT}/login/`;

    public PRODUCT_TYPE = `${this.SERVER_ROOT}/v1/product_type/`;
    public PRODUCT = `${this.SERVER_ROOT}/v1/product/`;
    public PRODUCT_VARIATION = `${this.SERVER_ROOT}/v1/product_variation/`;
}
